def lines(game_grid,n):
    s=''
    for i in range(n):
        s+=' ='+long_value(game_grid)*'='+'='
    return s

def long_value(game_grid):  #the length of the biggest data in the grid
    return len(str(max([max(game_grid[i]) for i in range(len(game_grid))])))

def grid_to_string(game_grid,n):
    s=''
    for x in game_grid:
        s+=lines(game_grid,n)
        s+=' \n'
        for y in x:
            s+='| '+str(y)+' '
        s+='|\n'
    s+=lines(game_grid,n)+' '
    return s


def grid_to_string_with_size(game_grid,n):
     s=''
     for x in game_grid:
        s+=lines(game_grid,n)
        s+='\n'
        s+='| '
        for y in x:
            if y==0:
                y=' '
            s+=str(y)+(long_value(game_grid)-len(str(y)))*' '+' | '
        s+='\n'
     s+=lines(game_grid,n)
     return s

THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}

def long_value_with_theme(grid,THEMES):
    long=0
    for x in grid:
       for y in x:
        if len(THEMES[y])>long:
            long=len(THEMES[y])
    return long

def grid_to_string_with_size_and_theme(game_grid,THEMES,n):
    length=long_value_with_theme(game_grid,THEMES)      ## longest word
    s=''
    for x in game_grid:
        for i in range(n):
            s+= '='+length*'='
        s+= '='
        s+='\n'
        s+='|'
        for y in x:
            s+=THEMES[y]+(length-len(THEMES[y]))*' '+'|'
        s+='\n'
    for i in range(n):      ## last line
        s+='='+length*'='
    s+='='
    return s
