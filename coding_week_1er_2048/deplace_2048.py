def move_row_left(one_list):
    list_positive=[i for i in one_list if i]
    list_0=[0]*(len(one_list)-len(list_positive))
    list_new=list_positive+list_0#put all the 0 in the back of the list
    for i in range(len(list_new)-1):
        if list_new[i]==list_new[i+1]:
            list_new[i]*=2
            list_new.pop(i+1)
            list_new.append(0)
    return list_new

def move_row_right(onelist):
    list_reversed=onelist[::-1]# we reversed the list
    return move_row_left(list_reversed)[::-1]

def game_grid_tranforme(game_grid):#we transforme the matrix
    game_grid_new=[]
    game_list=[]
    for row in range(len(game_grid)):
        for column in range(len(game_grid)):
            game_list+=[game_grid[column][row]]
    for i in range(len(game_grid)):
        game_grid_new.append(game_list[len(game_grid)*i:len(game_grid)*(i+1)])
    return game_grid_new

def move_grid(game_grid,direction):
    if direction=='Left':
        for list_index in range(len(game_grid)):
            game_grid[list_index]=move_row_left(game_grid[list_index])
    if direction=='Right':
        for list_index in range(len(game_grid)):
            game_grid[list_index]=move_row_right(game_grid[list_index])
    if direction=='Up':
        game_grid=game_grid_tranforme(game_grid)
        for list_index in range(len(game_grid)):
            game_grid[list_index]=move_row_left(game_grid[list_index])
        game_grid=game_grid_tranforme(game_grid)
    if direction=='Down':
        game_grid=game_grid_tranforme(game_grid)
        for list_index in range(len(game_grid)):
            game_grid[list_index]=move_row_right(game_grid[list_index])
        game_grid=game_grid_tranforme(game_grid)
    return game_grid

