from pytest import *
from coding_week_1er_2048.textual_2048 import read_player_command

def test_read_player_command(monkeypatch):
    def mock_input_return(request):
        return 'g'
    monkeypatch.setattr('builtins.input',mock_input_return)
    assert read_player_command() == 'g'

    def mock_input_return(request):
        return 'h'
    monkeypatch.setattr('builtins.input',mock_input_return)
    assert read_player_command() == 'h'

