from tkinter import *
from coding_week_1er_2048.grid_2048 import *
from coding_week_1er_2048.la_fin_2048 import *
from tkinter.messagebox import *

THEMES = {'Default': {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"},
          'Chemistry': {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"},
          'Alphabet': {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}
          }

TILES_BG_COLOR = {0: "#9e948a", 2: "#eee4da", 4: "#ede0c8", 8: "#f1b078",
                  16: "#eb8c52", 32: "#f67c5f", 64: "#f65e3b",
                  128: "#edcf72", 256: "#edcc61", 512: "#edc850",
                  1024: "#edc53f", 2048: "#edc22e", 4096: "#5eda92",
                  8192: "#24ba63"}

TILES_FG_COLOR = {0: "#776e65", 2: "#776e65", 4: "#776e65", 8: "#f9f6f2",
                  16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2",
                  256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2",
                  2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}

TILES_FONT = {"Verdana", 40, "bold"}

def start_guide():
    global guide_windows
    guide_windows = Tk()
    guide_windows.title('2048')
    Label(guide_windows, text="Let's Play 2048",font = "Helvetica 16 bold italic").grid()
    size_label = Label(guide_windows, text='Choose grid size')
    size_label.grid()

    size_init = IntVar()
    global size_enter
    size_enter = Spinbox(guide_windows, from_=0, to=100, increment=1, textvariable=size_init,width=5)
    size_init.set(4)
    size_enter.grid()

    theme_label = Label(guide_windows, text='Choose a theme')
    theme_label.grid()

    theme_list = ['Default', 'Chemistry', 'Alphabet']
    spinbox_list = StringVar(value=theme_list)
    global theme_choose
    theme_choose = Listbox(guide_windows, height=len(theme_list), listvariable=spinbox_list, selectmode=BROWSE,width=10)
    theme_choose.select_set(0)
    theme_choose.grid()

    play_button = Button(guide_windows, text='Play', command=start_play)
    play_button.grid()
    quit_button = Button(guide_windows, text='Quit', command=guide_windows.quit)
    quit_button.grid()

    guide_windows.mainloop()
    exit(1)


def start_play():
    global play_windows, play_windows_frame
    play_windows = Toplevel()
    play_windows.title('2048')
    play_windows_frame = Frame(play_windows)

    global size, theme, direction, graphical_grid
    direction = ' '
    size = int(size_enter.get())
    theme = theme_choose.get(theme_choose.curselection())
    graphical_grid = []
    graphical_grid_init()

    play_windows.mainloop()


def graphical_grid_init():
    global graphical_grid
    graphical_grid = init_game(size)

    global label_list
    label_list = []

    for i in range(size):
        for j in range(size):
            frame_cell = Frame(play_windows_frame,relief=SOLID,borderwidth=1)
            frame_cell.grid(row=i, column=j)
            label = Label(frame_cell, width=10, height=5,
                          text=THEMES[theme][graphical_grid[i][j]] if graphical_grid[i][j] != 0 else ' ',
                          bg=TILES_BG_COLOR[graphical_grid[i][j]],
                          fg=TILES_FG_COLOR[graphical_grid[i][j]])
            label.grid()
            label_list.append(label)

    play_windows_frame.focus_set()
    play_windows_frame.grid()
    play_windows_frame.bind('<Key>', key_pressed)

    return graphical_grid


def display_and_update_graphical_grid():
    global graphical_grid
    graphical_grid = move_grid(graphical_grid, direction)
    graphical_grid = grid_add_new_tile(graphical_grid)
    for i in range(size):
        for j in range(size):
            label_list[i * size + j].configure(
                text=THEMES[theme][graphical_grid[i][j]] if graphical_grid[i][j] != 0 else ' ',
                bg=TILES_BG_COLOR[graphical_grid[i][j]],
                fg=TILES_FG_COLOR[graphical_grid[i][j]])
    if is_win(graphical_grid):
        showinfo('Result_2048','YOU WIN')
    elif is_game_over(graphical_grid):
        showinfo('Result_2048','YOU LOSE')
    play_windows_frame.bind('<Key>', key_pressed)

def key_pressed(event):
    global direction
    direction_list = {0: 'Left', 1: 'Right', 2: 'Up', 3: 'Down'}
    direction_possible = [direction_list[i] for i in range(4) if move_possible(graphical_grid)[i] == True]
    if event.keysym in direction_possible:
        direction = event.keysym
        display_and_update_graphical_grid()
        direction = ' '
    else:
        play_windows_frame.bind('<Key>', key_pressed)

start_guide()
