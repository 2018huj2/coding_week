from coding_week_1er_2048.grid_2048 import get_empty_tiles_positions
from coding_week_1er_2048.deplace_2048 import move_row_left,move_row_right,move_grid,game_grid_tranforme

def is_grid_full(game_grid):
    if get_empty_tiles_positions(game_grid) ==[]:
        return True
    else:
        return False

def move_possible(game_grid):
    copy_game_grid=game_grid.copy()
    move_possible_direction=[]
    if move_grid(copy_game_grid,'Left')==game_grid:
        move_possible_direction.append(False)
    else:
        move_possible_direction.append(True)
    copy_game_grid=game_grid.copy()
    if move_grid(copy_game_grid,'Right')==game_grid:
        move_possible_direction.append(False)
    else:
        move_possible_direction.append(True)
    copy_game_grid=game_grid.copy()
    if move_grid(copy_game_grid,'Up')==game_grid:
        move_possible_direction.append(False)
    else:
        move_possible_direction.append(True)
    copy_game_grid=game_grid.copy()
    if move_grid(copy_game_grid,'Down')==game_grid:
        move_possible_direction.append(False)
    else:
        move_possible_direction.append(True)
    return move_possible_direction

def is_game_over(game_grid):
    if move_possible(game_grid)==[False,False,False,False]:
        return True
    else:
        return False

def get_grid_tile_max(game_grid):
    return max([max(game_grid[i]) for i in range(len(game_grid))])

def is_win(game_grid):
    if is_game_over(game_grid) and get_grid_tile_max(game_grid)>=2048:
        return True
    else:
        return False
