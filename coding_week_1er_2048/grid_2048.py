import random

def create_grid(grid_taille=4):
    game_gird = []
    for i in range(0,grid_taille):
        game_gird.append([0]*grid_taille)
    return game_gird

def get_value_new_tile():
    return random.choice([2,2,2,2,2,2,2,2,2,4])

def grid_add_new_tile_at_position(game_grid,m,n):
    game_grid[m][n]=get_value_new_tile()
    return game_grid

def get_all_tiles(game_grid):
    all_tiles=[]
    for row in range(len(game_grid)):
        for column in range(len(game_grid)):
            if game_grid[row][column]==0:
                all_tiles.append(0)
            else:
                all_tiles.append(game_grid[row][column])
    return all_tiles

def get_empty_tiles_positions(game_grid):
    empty_tiles_positions=[]
    for row in range(len(game_grid)):
        for column in range(len(game_grid)):
            if game_grid[row][column]==0:
                empty_tiles_positions.append((row,column))
    return empty_tiles_positions

def grid_get_value(game_grid,m,n):
    return game_grid[m][n]

def get_new_position(game_grid):
    return random.choice(get_empty_tiles_positions(game_grid))

def grid_add_new_tile(game_grid):
    m,n=get_new_position(game_grid)
    grid_add_new_tile_at_position(game_grid,m,n)
    return game_grid

def init_game(n=4):
    game_grid=create_grid(n)
    for i in range(2):
        grid_add_new_tile(game_grid)
    return game_grid

