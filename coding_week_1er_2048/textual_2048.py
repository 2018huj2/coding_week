from coding_week_1er_2048.grid_2048 import *
from coding_week_1er_2048.afficher_2048 import *
from coding_week_1er_2048.la_fin_2048 import *
import random as rd

def read_player_command():
    while 1:
        direction  = input('Entrez votre commande (g (gauche), d (droite), h (haut), b(bas)):')
        if direction   in ['g', 'd', 'h', 'b']:
            print('the direction is', direction )
            verifier = input('Entrez encore une fois pour verifier:')
            if verifier == direction:
                return direction
            else:
                print('Entrez encore une fois')
                continue
        else:
            print('Entrez correctement SVP')


def read_size_grid():
    while 1:
        size = input("Entrez la taille de grid (un numéro):")
        if size.isdigit():
            print('the size is', size)
            verifier = input('is that right? (y for yes):')
            if verifier == 'y':
                return size
            else:
                print('Entrez encore une fois')
                continue
        else:
            print('Entrez correctement SVP')


def read_theme_grid():
    theme_list = {'0': 'default', '1': 'chemistry', '2': 'alphabet'}
    while 1:
        theme = input("Entrez le thème (0 (default), 1 (chemistry), 2 (alphabet)):")
        if theme  in ['0', '1', '2']:
            print('the theme is', theme_list[theme])
            verifier = input('is that right? (y for yes):')
            if verifier == 'y':
                return theme
            else:
                print('Entrez encore une fois')
                continue
        else:
            print('Entrez correctement SVP')

def random_play(): # lance par PC
    game_grid = init_game(4)
    print(grid_to_string_with_size(game_grid, 4))
    move = {0: 'left', 1: 'right', 2: 'up', 3: 'down'}
    while not is_game_over(game_grid):
        move_list = []
        for i in range(4):
            if move_possible(game_grid)[i] == True:
                move_list.append(move[i])
        direction = rd.choice(move_list)
        print(direction, move_list)
        game_grid = move_grid(game_grid, direction)
        game_grid = grid_add_new_tile(game_grid)
        print(grid_to_string_with_size(game_grid, 4))
    if is_win(game_grid):
        print('you win')
    else:
        print('try again')

def game_play():  # joue par joueur
    size = int(read_size_grid())
    theme = read_theme_grid()
    game_grid = init_game(size)
    print(grid_to_string_with_size_and_theme(game_grid, THEMES[theme], size))
    move_list = {0: 'Left', 1: 'Right', 2: 'Up', 3: 'Down'}
    direction_list = {'g': 'Left', 'd': 'Right', 'h': 'Up', 'b': 'Down'}
    while not is_game_over(game_grid):
        move = []
        for i in range(4):
            if move_possible(game_grid)[i] == True:
                move.append(move_list[i])
        print('please choose one direction available:', ' '.join(move))
        direction = read_player_command()
        game_grid = move_grid(game_grid, direction_list[direction])
        game_grid = grid_add_new_tile(game_grid)
        print(grid_to_string_with_size_and_theme(game_grid, THEMES[theme], size))
    if is_win(game_grid):
        print('you win')
    else:
        print('try again')

#read_player_command()
#read_size_grid()
#read_theme_grid()
if __name__ == '__main__':
    game_play()
    exit(1)

