from coding_week_1er_2048.la_fin_2048 import is_grid_full,move_possible,is_game_over,get_grid_tile_max,is_win

def test_is_grid_full():
    assert is_grid_full([[16,4,8,2], [2,4,2,128], [4,512,32,64], [1024,2048,512,2]])
    assert not is_grid_full([[0,4,8,2], [0,0,0,0], [0,512,32,64], [1024,2048,512,0]])
test_is_grid_full()
def test_move_possible():
    assert move_possible([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]]) == [True,True,True,True]
    assert move_possible([[2, 4, 8, 16], [16, 8, 4, 2], [2, 4, 8, 16], [16, 8, 4, 2]]) == [False,False,False,False]
test_move_possible()
def test_is_game_over():
    assert is_game_over([[2, 4, 8, 16], [16, 8, 4, 2], [2, 4, 8, 16], [16, 8, 4, 2]])
    assert not is_game_over([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]])
test_is_game_over()
def test_get_grid_tile_max():
    assert get_grid_tile_max([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]])==32
    assert get_grid_tile_max([[16,4,8,2], [2,4,2,128], [4,512,32,64], [1024,2048,512,2]])==2048
test_get_grid_tile_max()

def test_is_win():
    assert is_win([[2, 4, 8, 16], [16, 8, 4, 2], [2, 4, 8, 16], [16, 8, 4, 2048]])
    assert not is_win([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]])
test_is_win()
